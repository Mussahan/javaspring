package com.example.akaWaster.Models;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.validation.constraints.*;

@Entity
public class Student
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @NotBlank(message="Name is required")
    @Size(max = 20, min = 3, message = "Student name invalid")
    private String studentName;
    @NotNull(message = "Age is required")
    private long studentAge;
    @NotBlank(message = "Email is required")
    @Pattern(regexp = "^(.+)@(.+)$",message = "Not valid email format")
    private String studentEmail;
    @NotBlank(message="Phone Number is required")
    @Pattern(regexp="^\\(?([0-9]{3})\\)?[-.\\\\s]?([0-9]{3})[-.\\\\s]?([0-9]{4})$",message = "Invalid phone number")
    private String phoneNumber;
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getStudentName() {

        return studentName;
    }
    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public Long getStudentAge() {
        return studentAge;
    }
    public void setStudentAge(Long studentAge) {

        this.studentAge = studentAge;
    }

    public String getStudentEmail() {

        return studentEmail;
    }
    public void setStudentEmail(String studentEmail) {

        this.studentEmail = studentEmail;
    }

    public String getPhoneNumber() {

        return phoneNumber;
    }
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}

package com.example.akaWaster.Models;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigInteger;
import java.util.List;

@Entity
//@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})

public class Books {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @NotBlank(message="Name is required")
    private String bookName;
    @NotNull(message = "Age is required")
    private long bookYear;
    private String bookUrl;
    @NotBlank(message = "Email is required")
    private String bookAuthor;
    @NotBlank(message="Phone Number is required")
    private String bookInformation;
    @Column(name = "book_genre")
    private Long bookGenre;

//    @Column(name="g enre_list_id")
//    private BigInteger bookPk;
//
//    public BigInteger getBookPk() {
//        return bookPk;
//    }
//
//    public void setBookPk(BigInteger bookPk) {
//        this.bookPk = bookPk;
//    }

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="book_genre",referencedColumnName="id",insertable = false,updatable = false)
    private GenreList genreList;

    public Long getBookGenre() {
        return bookGenre;
    }

    public void setBookGenre(Long bookGenre) {
        this.bookGenre = bookGenre;
    }

    public GenreList getGenreList() {
        return genreList;
    }

    public void setGenreList(GenreList genreList) {
        this.genreList = genreList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public long getBookYear() {
        return bookYear;
    }

    public void setBookYear(long bookYear) {
        this.bookYear = bookYear;
    }

    public String getBookAuthor() {
        return bookAuthor;
    }

    public void setBookAuthor(String bookAuthor) {
        this.bookAuthor = bookAuthor;
    }

    public String getBookInformation() {
        return bookInformation;
    }

    public void setBookInformation(String bookInformation) {
        this.bookInformation = bookInformation;
    }

    public String getBookUrl() { return bookUrl; }

    public void setBookUrl(String bookUrl) { this.bookUrl = bookUrl; }

}

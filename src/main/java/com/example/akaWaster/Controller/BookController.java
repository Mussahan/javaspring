package com.example.akaWaster.Controller;

import com.example.akaWaster.Models.Books;
import com.example.akaWaster.Repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Controller
public class BookController {
    private BookRepository bookRepository;
    @Autowired
    public void setBookRepository(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @RequestMapping(path = "/", method = RequestMethod.GET)
    public String getAllBooks(Model model) {
        model.addAttribute("books",bookRepository.findAll());
        return "index1";
    }

    @RequestMapping(path = "/books/add", method = RequestMethod.GET)
    public String createBooks(Model model) {
        model.addAttribute("books", new Books());
        return "addbooks";
    }

    @RequestMapping(path = "/", method = RequestMethod.POST)
    public String saveBook(@Valid Books books, Errors errors) {
        if(errors.hasErrors())
        { return "addbooks"; }
        bookRepository.save(books);
        return "index1";
    }
}

package com.example.akaWaster.Controller;

import com.example.akaWaster.Models.Books;
import com.example.akaWaster.Models.GenreList;
import com.example.akaWaster.Models.Student;
import com.example.akaWaster.Repository.BookRepository;
import com.example.akaWaster.Repository.GenreRepository;
import com.example.akaWaster.Repository.StudentRepository;
import com.example.akaWaster.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/api",produces = "application/json")
public class ApiController {

    private BookRepository bookRepository;
    private UserRepository userRepository;
    private StudentRepository studentRepository;
    private GenreRepository genreRepository;

    @Autowired

    public ApiController(BookRepository bookRepository, UserRepository userRepository, StudentRepository studentRepository,
                         GenreRepository genreRepository) {
        this.bookRepository = bookRepository;
        this.userRepository = userRepository;
        this.studentRepository = studentRepository;
        this.genreRepository = genreRepository;
    }

    @CrossOrigin
    @RequestMapping(path="/books/all",method = RequestMethod.GET)
    public List<Books> findAll(){
        return bookRepository.findAllPages();
    }

    //books
    @CrossOrigin
    @RequestMapping(path = "/books", method = RequestMethod.GET)
    public Page<Books> findAllByPage(@RequestParam Optional<String> bookName,
                              @RequestParam Optional<Integer> page,
                              @RequestParam Optional<String> sortBy){


        return bookRepository.findByBookName(bookName.orElse("_"),new PageRequest(page.orElse(0),5,
                Sort.Direction.ASC,sortBy.orElse("id")));

//        return bookRepository.findAll(new PageRequest(page.orElse(0),5,
//                Sort.Direction.ASC,sortBy.orElse("id")));
    }

    @CrossOrigin
    @PostMapping(value = "/books",consumes = "application/json")
    public Optional<Books> createBook(@RequestBody Books books) {
        System.out.println(books);
        bookRepository.save(books);
        return bookRepository.findById(books.getId());
    }

    @RequestMapping(path = "/books")
    public ResponseEntity handleFileUpload(@RequestParam("file") MultipartFile file) {
        try {
            System.out.printf("File name=%s, size=%s\n", file.getOriginalFilename(),file.getSize());
            //creating a new file in some local directory
            File fileToSave = new File("C:\\test\\" + file.getOriginalFilename());
            //copy file content from received file to new local file
            file.transferTo(fileToSave);
        } catch (IOException ioe) {
            //if something went bad, we need to inform client about it
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        //everything was OK, return HTTP OK status (200) to the client
        return ResponseEntity.ok().build();
    }


    @CrossOrigin
    @RequestMapping(path = "/books/delete/{id}", method = RequestMethod.GET)
    public String deleteBook(@PathVariable(name = "id") Long id) {
        bookRepository.deleteById(id);
        return "redirect:/books";
    }

    //Student
    @CrossOrigin
    @RequestMapping(path = "/student", method = RequestMethod.GET)
    public Page<Student> findStudnets(@RequestParam Optional<String> studentName,
                                      @RequestParam Optional<Integer> page,
                                      @RequestParam Optional<String> sortBy) {
        return studentRepository.findByStudentName(studentName.orElse("_"),new PageRequest(page.orElse(0),5,
                Sort.Direction.ASC,sortBy.orElse("id")));
    }

    @CrossOrigin
    @PostMapping(value = "/student/save",consumes = "application/json")
    public Optional<Student> creatStudent(@RequestBody Student student) {
        studentRepository.save(student);
        return studentRepository.findById(student.getId());
    }

    @CrossOrigin
    @RequestMapping(path = "/student/delete/{id}", method = RequestMethod.GET)
    public String deleteStudent(@PathVariable(name = "id") Long id){
        studentRepository.deleteById(id);
        return "redirect:/student";
    }

    //Genre
    @CrossOrigin
    @RequestMapping(path = "/genres",method = RequestMethod.GET)
    public List<GenreList> getGenreList(){
        return genreRepository.findAll();
    }

    @CrossOrigin
    @RequestMapping(path = "/books/genres/{id}",method = RequestMethod.GET)
    public List<Books> getBooksGenre(@PathVariable(name="id") Long genreName,
                                     @RequestParam Optional<Integer> page,
                                     @RequestParam Optional<String> sortBy) {

        return bookRepository.findAllByBookGenre(genreName, new PageRequest(page.orElse(0),5,
                        Sort.Direction.ASC,sortBy.orElse("id")));
    }

    @CrossOrigin
    @RequestMapping(path = "/students/all",method = RequestMethod.GET)
    public List<Student> getStudentsList(){
        return studentRepository.findAllStudents();
    }

}

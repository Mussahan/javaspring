package com.example.akaWaster.Controller;

import com.example.akaWaster.Models.Student;
import com.example.akaWaster.Repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@Controller
public class StudentController
{
    private StudentRepository studentRepository;
    @Autowired
    public void setStudentRepository(StudentRepository studentRepository){
        this.studentRepository = studentRepository;
    }

    @RequestMapping(path = "/students/add", method = RequestMethod.GET)
    public String createStudent(Model model) {
        model.addAttribute("student", new Student());
        return "addstudent";
    }

    @RequestMapping(path = "students", method = RequestMethod.POST)
    public String saveStudent(@Valid Student student, Errors errors) {
        if(errors.hasErrors())
        { return "addstudent"; }
        studentRepository.save(student);
        return "students";
    }
    @RequestMapping(path = "/students", method = RequestMethod.GET)
    public String getAllStudents(Model model) {
        model.addAttribute("student",studentRepository.findAll());
        return "students";
    }

    @RequestMapping(path = "/students/delete/{id}", method = RequestMethod.GET)
    public String deleteProduct(@PathVariable(name = "id") Long id) {
        studentRepository.deleteById(id);
        return "redirect:/students";
    }


    @RequestMapping(path = "/students/edit/{id}", method = RequestMethod.GET)
    public String editProduct(Model model, @PathVariable(value = "id") Long id) {

        model.addAttribute("student", studentRepository.findById(id));
        return "addstudent";
    }
}
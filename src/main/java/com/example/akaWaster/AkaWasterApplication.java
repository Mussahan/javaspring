package com.example.akaWaster;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AkaWasterApplication {

	public static void main(String[] args) {
		SpringApplication.run(AkaWasterApplication.class, args);
	}

}


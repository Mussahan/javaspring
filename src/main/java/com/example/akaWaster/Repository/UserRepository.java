package com.example.akaWaster.Repository;

import com.example.akaWaster.Models.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,Long>
{
    User findByUsername(String username);
}

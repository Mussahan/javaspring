package com.example.akaWaster.Repository;

import com.example.akaWaster.Models.GenreList;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GenreRepository extends JpaRepository<GenreList,Long> {

}

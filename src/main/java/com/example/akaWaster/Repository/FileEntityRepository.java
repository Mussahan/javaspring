package com.example.akaWaster.Repository;

import com.example.akaWaster.Models.FileEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FileEntityRepository extends JpaRepository<FileEntity, Long> {
}


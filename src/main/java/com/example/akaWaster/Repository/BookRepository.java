package com.example.akaWaster.Repository;

import com.example.akaWaster.Models.Books;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository

public interface BookRepository extends JpaRepository<Books,Long>
{
    @Query("SELECT b from Books b left join b.genreList where b.bookName like %?1%")
    Page<Books> findByBookName(String bookName, Pageable pageable );

//    Page<Books> findAll(Pageable pageable );

    @Query("select count(b) from Books b")
    List<Books> findAllPages();

    List<Books> findAllByBookGenre(Long bookGenre,Pageable pageable);

}

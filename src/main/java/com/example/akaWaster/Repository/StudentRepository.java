package com.example.akaWaster.Repository;

import com.example.akaWaster.Models.Student;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface StudentRepository extends JpaRepository<Student,Long>
{
    @Query("select s from Student s where studentName like %?1%")
    Page<Student> findByStudentName(String name,Pageable pageable);


    @Query("select count(s) from Student s")
    List<Student> findAllStudents();


}
